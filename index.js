var number1;
var number2;
var readyNum='';
var operator;

function changeClass(){
    presentClass=document.getElementById("entered").className;
    if(presentClass=="large"){
        document.getElementById("entered").className="small";
    }
    else{
        document.getElementById("entered").className="large";
    }
}

function setReadyNum(enteredNum){
    currentValue=document.getElementById("entered").value;
    enteredNum=enteredNum.toString(); 
    if(currentValue==0 || (readyNum=='' && number1===undefined && number2===undefined)){
        readyNum=enteredNum;
        if(document.getElementById("entered").className=="small")
            changeClass();
        document.getElementById("entered").value=readyNum;
    }
    else if(number1!=undefined && readyNum=='' && number2===undefined){
        if(document.getElementById("entered").className=="small")
            changeClass();
        document.getElementById("entered").value=number1.concat(operator).concat(enteredNum);
        readyNum=enteredNum;
    }
    else{
        readyNum=readyNum.concat(enteredNum);
        display=document.getElementById("entered").value;
        document.getElementById("entered").value=display+enteredNum;
    }   
}
function setNumbers(){
    if(readyNum!=''){
        if(number1===undefined){
            number1=readyNum;
        }
        else{
            number2=readyNum;
        }
    readyNum='';
    }
}

function setOperator(opr){
    setNumbers();
    if(number1===undefined){
        changeClass();
        document.getElementById("entered").value="Please Enter Number."
    }
    else if(number1!==undefined && operator==undefined){
        operator=opr;
        displayValue=document.getElementById("entered").value;
        document.getElementById("entered").value=displayValue+opr;
    }
    else if(operator!=undefined && number2===undefined){
        displayValue=document.getElementById("entered").value;
        document.getElementById("entered").value=displayValue.replace(operator,opr);
        operator=opr;
    }
    else if(operator!=undefined && number2!=undefined){
        changeClass();
        document.getElementById("entered").value="Only one operation can be performed."
    }
}

function compute(){
    setNumbers();
    if(number1!=undefined && operator!=undefined && number2===undefined){
        changeClass();
        document.getElementById("entered").value="Please Enter 2nd Number."
    }
    else if(number1==undefined && readyNum==''){
        document.getElementById("entered").value='0';
    }
    else{
        if(document.getElementById("entered").className=="small")
            changeClass();
        num1=Number(number1);
        num2=Number(number2);
        let result;
        if(operator=="+") 
            result=num1+num2;
        else if(operator=="-") 
            result=num1-num2;
        else if(operator=="*") 
            result=num1*num2;
        else if(operator=="/") 
            result=num1/num2;
        clearAll();
        document.getElementById("entered").value=result;
    }
}

function clearAll(){
    operator=undefined;
    number1=undefined;
    number2=undefined;
    readyNum='';
    if(document.getElementById("entered").className=="small")
        changeClass();
    document.getElementById("entered").value="0";
}